﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LawsuitsSchedule.Architecture.Helpers.Queries
{
    public interface IQueryHandler
    {
    }

    public interface IQueryHandler<TQuery> : IQueryHandler
        where TQuery : IQuery
    {
        TResult Execute<TResult>(TQuery query);
    }
}
