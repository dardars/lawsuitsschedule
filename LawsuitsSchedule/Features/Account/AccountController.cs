﻿using LawsuitsSchedule.Features.Account.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace LawsuitsSchedule.Features.Account
{
    public class AccountController : Controller
    {
        private readonly LawsuitsScheduleContext context = new LawsuitsScheduleContext();

        [HttpGet]
        public ActionResult Login()
        {
            var admin = (from a in context.Employes
                         where a.Email == "admin"
                         select a).FirstOrDefault();

            if(admin == null)
            {
                context.Addresess.Add(new Address()
                {
                    City = "",
                    Street = "",
                    HomeNumber = "",
                    FlatNumber = ""
                });

                context.Companies.Add(new Company()
                {
                    Name = ""
                });

                context.EmployeeTypes.Add(new EmployeeType()
                {
                    Name = "Administrator"
                });

                context.Employes.Add(new Employee()
                {
                    Name = "Admin",
                    LastName = "Administratorowski",
                    PhoneNumber = "123123123",
                    Email = "admin",
                    Password = "admin",
                });
                context.SaveChanges();
            }

            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel loginModel, string ReturnUrl)
        {
            if ((loginModel.Email == "admin" && loginModel.Password == "admin") || ModelState.IsValid)
            {
                using (context)
                {
                    var accounts = (from a in context.Employes
                                where a.Email == loginModel.Email
                                select a).FirstOrDefault();

                    

                    if (accounts != null && accounts.Password == loginModel.Password)
                    {
                        FormsAuthentication.SetAuthCookie(loginModel.Email, loginModel.Remember);

                        var type = (from a in context.EmployeeTypes
                                where a.TypeId == accounts.TypeId
                                select a).FirstOrDefault();

                        Session["Name"] = accounts.Name.ToString();
                        Session["TypeName"] = type.Name.ToString();

                        if (ReturnUrl == null)
                            return RedirectToAction("GetLawsuit", "Lawsuit");
                        else
                            return Redirect(ReturnUrl);
                    }
                    else
                    {
                        ViewBag.Error = "Źle wprowadzono adres email lub hasło";
                        return View(loginModel);
                    }
                }
            }
            else
            {
                return View(loginModel);
            }
        }

        public ActionResult Logout()
        {
            Session.RemoveAll();
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

    }
}