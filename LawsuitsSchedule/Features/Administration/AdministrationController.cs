﻿using LawsuitsSchedule.Features.Administration.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LawsuitsSchedule.Features.Administration
{
    [Authorize]
    public class AdministrationController : Controller
    {
        private readonly LawsuitsScheduleContext context = new LawsuitsScheduleContext();

        public ActionResult Panel()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddEmployee()
        {
            var getCompanyName = context.Companies.ToList();

            SelectList listCompanyName = new SelectList(getCompanyName, "CompanyId", "Name");
            ViewBag.listCompanyName = listCompanyName;
            return View();
        }

        [HttpPost]
        public ActionResult AddEmployee(AddAccountViewModel addAccountModel)
        {
            var getCompanyName = context.Companies.ToList();

            SelectList listCompanyName = new SelectList(getCompanyName, "CompanyId", "Name");
            ViewBag.listCompanyName = listCompanyName;

            if (ModelState.IsValid)
            {
                context.Addresess.Add(new Address()
                {
                    City = addAccountModel.City,
                    Street = addAccountModel.Street,
                    HomeNumber = addAccountModel.HomeNumber,
                    FlatNumber = addAccountModel.FlatNumber
                });

                var id = context.EmployeeTypes.FirstOrDefault(x => x.Name == "Pracownik");

                if (id == null)
                {
                    context.EmployeeTypes.Add(new EmployeeType()
                    {
                    });

                    if (addAccountModel.ChooseCheck == true)
                    {
                        context.Companies.Add(new Company()
                        {
                            Name = addAccountModel.CompanyName
                        });

                        context.Employes.Add(new Employee()
                        {
                            Name = addAccountModel.Name,
                            LastName = addAccountModel.Surname,
                            PhoneNumber = addAccountModel.Telephone,
                            Email = addAccountModel.Email,
                            Password = addAccountModel.Password,
                        });
                    }
                    else
                    {
                        context.Employes.Add(new Employee()
                        {
                            Name = addAccountModel.Name,
                            LastName = addAccountModel.Surname,
                            PhoneNumber = addAccountModel.Telephone,
                            Email = addAccountModel.Email,
                            Password = addAccountModel.Password,
                            CompanyId = addAccountModel.CompanyID.Value
                        });
                    }
                }
                else
                {
                    if (addAccountModel.ChooseCheck == true)
                    {
                        context.Companies.Add(new Company()
                        {
                            Name = addAccountModel.CompanyName
                        });

                        context.Employes.Add(new Employee()
                        {
                            Name = addAccountModel.Name,
                            LastName = addAccountModel.Surname,
                            PhoneNumber = addAccountModel.Telephone,
                            Email = addAccountModel.Email,
                            Password = addAccountModel.Password,
                            TypeId = id.TypeId
                        });
                    }
                    else
                    {
                        context.Employes.Add(new Employee()
                        {
                            Name = addAccountModel.Name,
                            LastName = addAccountModel.Surname,
                            PhoneNumber = addAccountModel.Telephone,
                            Email = addAccountModel.Email,
                            Password = addAccountModel.Password,
                            CompanyId = addAccountModel.CompanyID.Value,
                            TypeId = id.TypeId
                        });
                    }
                }

                context.SaveChanges();

                ModelState.Clear();

                ViewBag.Name = addAccountModel.Name;

                return View();
            }
            else
            {
                return View(addAccountModel);
            }
        }

        [HttpGet]
        public ActionResult EditEmployee()
        {
            var getCompanyName = context.Companies.ToList();
            var getEmployees = context.Employes.ToList();

            SelectList listEmployees = new SelectList(getEmployees, "EmployeeId", "Name", "LastName");
            SelectList listCompanyName = new SelectList(getCompanyName, "CompanyId", "Name");

            ViewBag.listEmployees = listEmployees;
            ViewBag.listCompanyName = listCompanyName;
            return View();
        }

        [HttpPost]
        public ActionResult EditEmployee(AddAccountViewModel addAccountModel)
        {
            var getCompanyName = context.Companies.ToList();
            var getEmployees = context.Employes.ToList();

            SelectList listCompanyName = new SelectList(getCompanyName, "CompanyId", "Name");
            SelectList listEmployees = new SelectList(getEmployees, "EmployeeId", "Name", "LastName");

            ViewBag.listEmployees = listEmployees;
            ViewBag.listCompanyName = listCompanyName;

            var employee = context.Employes.Where(x => x.EmployeeId == addAccountModel.EmployeeId).FirstOrDefault();

            if (ModelState.IsValid)
            {
                context.Addresess.Add(new Address()
                {
                    City = addAccountModel.City,
                    Street = addAccountModel.Street,
                    HomeNumber = addAccountModel.HomeNumber,
                    FlatNumber = addAccountModel.FlatNumber
                });

                var id = context.EmployeeTypes.FirstOrDefault(x => x.Name == "Pracownik");

                if (id == null)
                {
                    context.EmployeeTypes.Add(new EmployeeType()
                    {
                    });

                    if (addAccountModel.ChooseCheck == true)
                    {
                        context.Companies.Add(new Company()
                        {
                            Name = addAccountModel.CompanyName
                        });

                        employee.Name = addAccountModel.Name;
                        employee.LastName = addAccountModel.Surname;
                        employee.PhoneNumber = addAccountModel.Telephone;
                        employee.Email = addAccountModel.Email;
                        employee.Password = addAccountModel.Password;
                        
                    }
                    else
                    {
                        employee.Name = addAccountModel.Name;
                        employee.LastName = addAccountModel.Surname;
                        employee.PhoneNumber = addAccountModel.Telephone;
                        employee.Email = addAccountModel.Email;
                        employee.Password = addAccountModel.Password;
                        employee.CompanyId = addAccountModel.CompanyID.Value;
                    }
                }
                else
                {
                    if (addAccountModel.ChooseCheck == true)
                    {
                        context.Companies.Add(new Company()
                        {
                            Name = addAccountModel.CompanyName
                        });

                        employee.Name = addAccountModel.Name;
                        employee.LastName = addAccountModel.Surname;
                        employee.PhoneNumber = addAccountModel.Telephone;
                        employee.Email = addAccountModel.Email;
                        employee.Password = addAccountModel.Password;
                        employee.TypeId = id.TypeId;
                    }
                    else
                    {
                        employee.Name = addAccountModel.Name;
                        employee.LastName = addAccountModel.Surname;
                        employee.PhoneNumber = addAccountModel.Telephone;
                        employee.Email = addAccountModel.Email;
                        employee.Password = addAccountModel.Password;
                        employee.CompanyId = addAccountModel.CompanyID.Value;
                        employee.TypeId = id.TypeId;
                    }
                }

                context.SaveChanges();

                ModelState.Clear();

                ViewBag.Name = addAccountModel.Name;

                return View();
            }
            else
            {
                return View(addAccountModel);
            }
        }

        [HttpGet]
        public ActionResult AddCompany()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddCompany(LawsuitsSchedule.Company company)
        {
            context.Companies.Add(company);
            context.SaveChanges();

            ModelState.Clear();

            return View();
        }

    }
}