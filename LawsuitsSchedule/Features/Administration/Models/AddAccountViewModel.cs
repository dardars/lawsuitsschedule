﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LawsuitsSchedule.Features.Administration.Models
{
    public class AddAccountViewModel
    {
        [Required(ErrorMessage = "Proszę podać imię.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Proszę podać nazwisko.")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Proszę podać numer telefonu.")]
        public string Telephone { get; set; }

        [Required(ErrorMessage = "Proszę podać adres e-mail.")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Proszę podać prawidłowy adres e-mail.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Proszę podać hasło.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Proszę potwierdzić hasło.")]
        [Compare("Password", ErrorMessage = "Potwierdzone hasło nie zgadza się z ustalonym hasłem.")]
        public string ConfirmPassword { get; set; }

        public Nullable<int> CompanyID { get; set; }

        public Nullable<int> EmployeeId { get; set; }

        public bool ChooseCheck { get; set; } 

        //Jeżeli z listy rozwijanej nie będzie danej firmy to wykona się CompanyName
        //[Required(ErrorMessage = "Proszę podać nazwę firmy.")]
        public string CompanyName { get; set; }

        //Adres zamieszkania
        [Required(ErrorMessage = "Proszę podać miasto.")]
        public string City { get; set; }

        [Required(ErrorMessage = "Proszę podać ulicę.")]
        public string Street { get; set; }

        [Required(ErrorMessage = "Proszę podać numer domu.")]
        public string HomeNumber { get; set; }

        [Required(ErrorMessage = "Proszę podać numer lokalu.")]
        public string FlatNumber { get; set; }
    }
}