﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LawsuitsSchedule.Features.Cases.Algoritm
{
    public static class CaseSchedule
    {
        private static readonly LawsuitsScheduleContext context = new LawsuitsScheduleContext();
        private static double _startDayofWork;
        private static double _endDayofWork;
        private static double _caseLongTime;
        private static double _dayDelay;

        static CaseSchedule()
        {
            //context = new LawsuitsScheduleContext();
            //double.TryParse(context.BusinessParameters.FirstOrDefault(x => Equals(x.ParameterName, "StartDayOfWork")).ParameterValue,out _startDayofWork);
            //double.TryParse(context.BusinessParameters.FirstOrDefault(x => Equals(x.ParameterName, "EndDayOfWork")).ParameterValue,out _endDayofWork);
            //double.TryParse(context.BusinessParameters.FirstOrDefault(x => Equals(x.ParameterName, "CaseLongTime")).ParameterValue, out _caseLongTime);
            //double.TryParse(context.BusinessParameters.FirstOrDefault(x => Equals(x.ParameterName, "DayDelay")).ParameterValue, out _dayDelay);
            
            if (_dayDelay == 0)
                _dayDelay = 2;
            if (_caseLongTime == 0)
                _caseLongTime = 1;
            if (_startDayofWork == 0)
                _startDayofWork = 8;
            if (_endDayofWork == 0)
                _endDayofWork = 16;
        }

        public static void PlanDateHearing(int lawsuitKey)
        {
            DateTime delayedDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(_dayDelay); /*DateTime.Now.AddDays(_dayDelay);*/
            bool foundedFreeTime = false;
            while (!foundedFreeTime)
            {
                var actualCasesDayList = context.CaseScheduleCalendars.ToList().Where(x => Equals(x.HearingStart.Date, delayedDate.Date)).OrderBy(c=>c.HearingStart).ToList();
                if (FindFreeTime(actualCasesDayList, delayedDate, lawsuitKey))
                    return;
                delayedDate.AddDays(1);
            }
        }

        private static bool FindFreeTime(IList<CaseScheduleCalendar> list,DateTime delayedDate,int lawsuitKey)
        {
            switch (list.Count)
            {
                case 0:
                    ReserveTimeForDontExistCase(delayedDate, lawsuitKey);
                    return true;
                case 1:
                    ReserveTimeWhenExistOneCase(delayedDate, lawsuitKey);
                    return true;
                default:
                    ReserveTimeWhenExistMoreCase(delayedDate, lawsuitKey);
                    return true;
            }
        }

        private static void ReserveTimeForDontExistCase(DateTime delayedDate,int lawsuitKey)
        {
            var lawsuit = context.Lawsuits.FirstOrDefault(x => x.LawsuitsId== lawsuitKey);
            var hearingStart = delayedDate.AddHours(_startDayofWork);

            lawsuit.CalendarDateId = CreateCalendarElement(hearingStart, hearingStart.AddHours(_caseLongTime));
            context.SaveChanges();
        }

        private static void ReserveTimeWhenExistOneCase(DateTime delayedDate, int lawsuitKey)
        {
            var lawsuit = context.Lawsuits.FirstOrDefault(x => x.LawsuitsId== lawsuitKey);
            var hearingStart = delayedDate.AddHours(_startDayofWork);

            var lastCase = context.CaseScheduleCalendars.ToList().Where(x => x.HearingStart.Date== delayedDate.Date).FirstOrDefault();
            

            lawsuit.CalendarDateId = CreateCalendarElement(lastCase.HearingEnd, lastCase.HearingEnd.AddHours(_caseLongTime));
            context.SaveChanges();
        }
        private static void ReserveTimeWhenExistMoreCase(DateTime delayedDate, int lawsuitKey)
        {
            var lawsuit = context.Lawsuits.FirstOrDefault(x => x.LawsuitsId== lawsuitKey);
            var hearingStart = delayedDate.AddHours(_startDayofWork);
            var endDayofWorkHour = delayedDate.AddHours(_endDayofWork);
            int caseCalendarId = 0;

            var lastCase = context.CaseScheduleCalendars.ToList().Where(x => x.HearingStart.Date== delayedDate.Date)
                .OrderByDescending(c=>c.HearingEnd).FirstOrDefault();

            if (lastCase.HearingEnd.AddHours(_caseLongTime).TimeOfDay <= endDayofWorkHour.TimeOfDay) //jeśli tak można wrzucić tego samego dnia
            {
                caseCalendarId = CreateCalendarElement(lastCase.HearingEnd, lastCase.HearingEnd.AddHours(_caseLongTime));
            }
            else //jeśli nie, wrzucamy na następny dzień
            {
                ReserveTimeForDontExistCase(delayedDate.AddDays(1), lawsuitKey);
                return;
            }

            lawsuit.CalendarDateId = caseCalendarId;
            context.SaveChanges();
        }

        private static int CreateCalendarElement(DateTime start, DateTime end)
        {
            var caseCalendarEntity = context.CaseScheduleCalendars.Create();
            caseCalendarEntity.HearingStart = start;
            caseCalendarEntity.HearingEnd = end;

            context.CaseScheduleCalendars.Add(caseCalendarEntity);
            context.SaveChanges();

            return caseCalendarEntity.CalendarId;
        }
    }
}