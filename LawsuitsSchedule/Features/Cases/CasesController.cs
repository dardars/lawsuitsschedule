﻿using LawsuitsSchedule.Features.Cases.Algoritm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LawsuitsSchedule.Features.Cases
{
    public class CasesController : Controller
    {
        // GET: Cases
        public ActionResult Plan(int caseId)
        {
            CaseSchedule.PlanDateHearing(caseId);
            return RedirectToAction("GetLawsuit","Lawsuit");
        }
    }
}