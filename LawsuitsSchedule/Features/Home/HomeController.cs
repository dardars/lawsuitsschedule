﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LawsuitsSchedule.Features.Home
{
    public class HomeController : Controller
    {
        private readonly LawsuitsScheduleContext context;

        public HomeController()
        {
            context = new LawsuitsScheduleContext();
        }

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
    }
}