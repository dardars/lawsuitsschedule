﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using LawsuitsSchedule.Features.Lawsuit.Models;
using LawsuitsSchedule.Infrastructure;

namespace LawsuitsSchedule.Features.Lawsuit
{
    [Authorize]
    public class LawsuitController : Controller
    {
        private readonly LawsuitsScheduleContext context = new LawsuitsScheduleContext();

        public ActionResult MainLawsuit()
        {
            return View();
        }

        public ActionResult GetLawsuit()
        {
            List<LawsuitsSchedule.Lawsuit> lawsuit = null;

            using (context)
            {
                lawsuit = context.Lawsuits.ToList();
                lawsuit.ForEach(x =>
                {
                    x.Claimant = context.Clients.Where(z => z.ClientId == x.ClaimantId).FirstOrDefault();
                    x.Defendant = context.Clients.Where(z => z.ClientId == x.DefendantId).FirstOrDefault();
                    x.Judge = context.Employes.Where(z => z.EmployeeId == x.JudgeId).FirstOrDefault();
                    x.CalendarDate = context.CaseScheduleCalendars.Where(z => z.CalendarId == x.CalendarDateId).FirstOrDefault();
                });

            }
            return View(lawsuit);
        }

        [HttpGet]
        public ActionResult EditLawsuit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            LawsuitsSchedule.Lawsuit lawsuit = null;
            using (context)
            {
                lawsuit = context.Lawsuits.Where(x => x.LawsuitsId == id).FirstOrDefault();

                lawsuit.Claimant = context.Clients.Where(z => z.ClientId == lawsuit.ClaimantId).FirstOrDefault();
                lawsuit.Defendant = context.Clients.Where(z => z.ClientId == lawsuit.DefendantId).FirstOrDefault();
                lawsuit.Judge = context.Employes.Where(z => z.EmployeeId == lawsuit.JudgeId).FirstOrDefault();
                lawsuit.CalendarDate = context.CaseScheduleCalendars.Where(z => z.CalendarId == lawsuit.CalendarDateId).FirstOrDefault();
                GlobalRepository.DateLawsuit = lawsuit.LawsuitDate;

                GlobalRepository.JudgeId = lawsuit.JudgeId;

                var viewModel = new LawsuitViewModel()
                {
                    Lawsuit = lawsuit,
                    Employers = context.Employes.Where(x => x.EmployeeType.Name != "Admin").ToList()
                };

                if (viewModel.Lawsuit == null)
                {
                    return HttpNotFound();
                }

                return View(viewModel);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditLawsuit(LawsuitsSchedule.Lawsuit lawsuit)
        {
            if (ModelState.IsValid)
            {
                lawsuit.ClaimantId = lawsuit.Claimant.ClientId;
                lawsuit.DefendantId = lawsuit.Defendant.ClientId;
                lawsuit.JudgeId = GlobalRepository.JudgeId;
                lawsuit.LawsuitDate = GlobalRepository.DateLawsuit;
                context.Entry(lawsuit).State = EntityState.Modified;
                context.SaveChanges();
                TempData["Edit"] = lawsuit.Signature;

                return RedirectToAction("GetLawsuit");
            }

            GlobalRepository.JudgeId = null;
            GlobalRepository.DateLawsuit = DateTime.MinValue;

            return View(lawsuit);
        }

        public ActionResult AddEmployeeTo(int? id)
        {
            return View();
        }

        public ActionResult DetailsLawsuit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            LawsuitsSchedule.Lawsuit lawsuit = context.Lawsuits.Find(id);

            if (lawsuit == null)
            {
                return HttpNotFound();
            }

            return View(lawsuit);
        }

        public ActionResult PlanLawsuit(int? id)
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddLawsuit()
        {
            var viewModel = new LawsuitViewModel()
            {
                Lawsuit = context.Lawsuits.Create(),
                Employers = context.Employes.Where(x => x.EmployeeType.Name != "Admin").ToList()
            };
            //JudgeId
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddLawsuit(LawsuitsSchedule.Lawsuit lawsuit, string JudgeId = "1")
        {
            var clientDef = context.Clients.Create();
            var clientClay = context.Clients.Create();
            var employe = context.Employes.Create();
            var NewLawsuit = context.Lawsuits.Create();

            var addresId = CreateAdress();
            //var employeId = CreateEmploye(addresId, lawsuit);
            var defId = CreateDef(addresId, int.Parse(JudgeId), lawsuit);
            var clayId = CreateClay(addresId, int.Parse(JudgeId), lawsuit);


            NewLawsuit.ClaimantId = clayId;
            NewLawsuit.DefendantId = defId;
            NewLawsuit.JudgeId = int.Parse(JudgeId);
            NewLawsuit.Signature = lawsuit.Signature;
            NewLawsuit.LawsuitDate = DateTime.Now;

            context.Lawsuits.Add(NewLawsuit);
            context.SaveChanges();

            var viewModel = new LawsuitViewModel()
            {
                Lawsuit = context.Lawsuits.Create(),
                Employers = context.Employes.Where(x => x.EmployeeType.Name != "Admin").ToList()
            };
            ViewBag.Message = "Dodano sprawę";
            return View(viewModel);
        }

        private int CreateAdress()
        {
            var adress = context.Addresess.Create();
            adress.City = "Kraków";
            adress.FlatNumber = "23";
            adress.HomeNumber = "24";
            adress.Street = "Podwale";

            context.Addresess.Add(adress);
            context.SaveChanges();


            return adress.AddressId;
        }

        //private int CreateEmploye(int addresId, LawsuitsSchedule.Lawsuit lawsuit)
        //{
        //    var employe = context.Employes.Create();
        //    employe.AddressId = addresId;
        //    employe.CompanyId = 1;
        //    employe.Email = lawsuit.Judge.Email;
        //    employe.TypeId = 2;
        //    employe.LastName = lawsuit.Judge.LastName;
        //    employe.Name = lawsuit.Judge.Name;
        //    employe.Password = lawsuit.Judge.Password;
        //    employe.PhoneNumber = lawsuit.Judge.PhoneNumber;

        //    context.Employes.Add(employe);
        //    context.SaveChanges();

        //    return employe.EmployeeId;
        //}

        private int CreateClay(int addresId, int employeId, LawsuitsSchedule.Lawsuit lawsuit)
        {
            var clientClay = context.Clients.Create();
            clientClay.AddressId = addresId;
            clientClay.LawyerId = employeId;
            clientClay.LegalForm = lawsuit.Claimant.LegalForm;
            clientClay.Name = lawsuit.Claimant.Name;
            clientClay.Type = "Pozywajacy";

            context.Clients.Add(clientClay);
            context.SaveChanges();

            return clientClay.ClientId;
        }

        private int CreateDef(int addresId, int employeId, LawsuitsSchedule.Lawsuit lawsuit)
        {
            var clientDef = context.Clients.Create();
            clientDef.AddressId = addresId;
            clientDef.LawyerId = employeId;
            clientDef.LegalForm = lawsuit.Claimant.LegalForm;
            clientDef.Name = lawsuit.Claimant.Name;
            clientDef.Type = "Pozwany";

            context.Clients.Add(clientDef);
            context.SaveChanges();

            return clientDef.ClientId;
        }
    }
}