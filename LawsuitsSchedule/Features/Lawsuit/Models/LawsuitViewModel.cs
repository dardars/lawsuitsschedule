﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LawsuitsSchedule.Features.Lawsuit.Models
{
    public class LawsuitViewModel
    {
        public LawsuitsSchedule.Lawsuit Lawsuit { get; set; }
        public List<Employee> Employers { get; set; }
    }
}