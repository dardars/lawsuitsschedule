﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LawsuitsSchedule.Infrastructure
{
    public static class GlobalRepository
    {
        public static int? JudgeId { get; set; }
        public static DateTime DateLawsuit { get; set; }
    }
}