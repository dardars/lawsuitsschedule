﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LawsuitsSchedule.Infrastructure
{
    public class LocationViewEngine : RazorViewEngine
    {
        public LocationViewEngine()
        {
            ViewLocationFormats = new string[] {
                "~/Features/{1}/Views/{0}.cshtml"
            };
        }
    }
}