namespace LawsuitsSchedule
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity;
    using System.Linq;
    using System.Web.Mvc;

    public class LawsuitsScheduleContext : DbContext
    {
        // Your context has been configured to use a 'LawsuitsScheduleContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'LawsuitsSchedule.LawsuitsScheduleContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'LawsuitsScheduleContext' 
        // connection string in the application configuration file.
        public LawsuitsScheduleContext()
            : base("name=LawsuitsScheduleContext")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Lawsuit> Lawsuits { get; set; }
        public DbSet<Address> Addresess { get; set; }
        public DbSet<Employee> Employes { get; set; }
        public DbSet<EmployeeType> EmployeeTypes { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<BusinessParameter> BusinessParameters { get; set; }
        public DbSet<CaseScheduleCalendar> CaseScheduleCalendars { get; set; }


    }
    public class Client
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        public int ClientId { get; set; }
        [Display(Name = "Imie")]
        public string Name { get; set; }
        [Display(Name = "Fizyczny/Prawny")]
        public string LegalForm { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int LawyerId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int AddressId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string Type { get; set; }

        public List<Lawsuit> LawsuitsClaimant { get; set; }
        public List<Lawsuit> LawsuitsDefendant { get; set; }

        public Address Address { get; set; }
        public Employee Lawyer { get; set; }
    }
    public class Lawsuit
    {
        [Key]
        public int LawsuitsId { get; set; }
        [HiddenInput(DisplayValue =false)]
        public int? JudgeId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? ClaimantId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? DefendantId { get; set; }
        public string Signature { get; set; }
        [HiddenInput(DisplayValue = false)]
        public DateTime LawsuitDate { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? CalendarDateId { get; set; }

        public Client Claimant { get; set; }
        public Client Defendant { get; set; }
        public Employee Judge { get; set; }
        public CaseScheduleCalendar CalendarDate { get; set; }
    }
    public class Address
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        public int AddressId { get; set; }
        [Display(Name ="Miasto")]
        public string City { get; set; }
        [Display(Name = "Ulica")]
        public string Street { get; set; }
        [Display(Name = "Numer domu")]
        public string HomeNumber { get; set; }
        [Display(Name = "Numer mieszkania")]
        public string FlatNumber { get; set; }

        public List<Client> Clients { get; set; }
        public List<Employee> Employees { get; set; }
    }
    public class Employee
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        public int EmployeeId { get; set; }
        [Display(Name = "Imie pracownika")]
        public string Name { get; set; }
        [Display(Name = "Nazwisko pracownika")]
        public string LastName { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int CompanyId { get; set; }
        [Display(Name = "Numer telefonu")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Has�o")]
        public string Password { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int TypeId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int AddressId { get; set; }

        public List<Client> Clients { get; set; }
        public List<Lawsuit> Lawsuits { get; set; }

        public EmployeeType EmployeeType { get; set; }
        public Company Company { get; set; }
        public Address Address { get; set; }
    }
    public class EmployeeType
    {
        public EmployeeType()
        {
            Name = "Pracownik";
        }

        [Key]
        public int TypeId { get; set; }
        public string Name { get; set; }

        public List<Employee> Employees { get; set; }
    }
    public class Company
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        public int CompanyId { get; set; }
        [Display(Name = "Nazwa firmy")]
        public string Name { get; set; }

        public List<Employee> Employees { get; set; }
    }

    public class BusinessParameter
    {
        [Key]
        public int ParameterId { get; set; }
        public string ParameterName { get; set; }
        public string ParameterValue { get; set; }
    }

    public class CaseScheduleCalendar
    {
        [Key]
        public int CalendarId { get; set; }
        public DateTime HearingStart { get; set; }
        public DateTime HearingEnd { get; set; }
    }
}